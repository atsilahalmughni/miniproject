package com.example.basicbeans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BasicBeansApplication {

	public static void main(String[] args) {

		ApplicationContext apc = SpringApplication.run(BasicBeansApplication.class, args);
		for(String s : apc.getBeanDefinitionNames()){
			System.out.println(s);
		}
	}

	@Bean
	public String getname(){
		return "Andi";
	}

}
