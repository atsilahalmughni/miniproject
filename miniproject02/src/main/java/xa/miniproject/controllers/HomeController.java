package xa.miniproject.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/")
public class HomeController {
	
	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/home");
		return view;
	}
	
	@GetMapping(value="landingpage")
	public ModelAndView landingpage(HttpSession name) {
		ModelAndView view = new ModelAndView("/landingpage");
		view.addObject("userName", name.getAttribute("username"));
		return view;
	}

}
