package xa.miniproject.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xa.miniproject.models.Employee;
import xa.miniproject.repositories.EmployeeRepo;




@Controller
@RequestMapping(value="/employee/")

public class EmployeeController {
	
	@Autowired
	private EmployeeRepo employeerepo;
	
	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/employee/index");
		List<Employee> listemp= this.employeerepo.findAll();
		List<Employee> listsort = new ArrayList<Employee>();
		listsort = listemp.stream().sorted((f,s) -> f.getFirstname().compareTo(s.getFirstname())).collect(Collectors.toList());
		view.addObject("listsort", listsort);
		return view;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/employee/form");
		Employee employee = new Employee();
		view.addObject("employee", employee);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Employee employee, BindingResult result) {
		if(!result.hasErrors()) {
			this.employeerepo.save(employee);
		}
		return new ModelAndView("redirect:/employee/index");
	}
	
	@GetMapping(value="edit/{id}")
	public ModelAndView editfrom(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/employee/form");
		Employee employee = this.employeerepo.findById(id).orElse(null);
		view.addObject("employee", employee);
		return view;
	}
	
	@GetMapping(value="deleteform/{id}")
	public ModelAndView deletefrom(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/employee/deleteform");
		Employee employee = this.employeerepo.findById(id).orElse(null);
		view.addObject("employee", employee);
		return view;
	}
	
	@GetMapping(value="del/{id}")
	public ModelAndView del(@PathVariable ("id") Long id) {
		if(id != null) {
			this.employeerepo.deleteById(id);
			
		}
		return new ModelAndView("redirect:/employee/index");
	}

}
