package xa.miniproject.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xa.miniproject.models.Employee;
import xa.miniproject.repositories.EmployeeRepo;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/api/")
public class ApiEmployeeController {
	
	@Autowired EmployeeRepo employeerepo;
	
	@GetMapping("employee")
	public ResponseEntity<List<Employee>> getAllEmployee(){
		try {
			List<Employee> employee = this.employeerepo.findAll();
			List<Employee> listsort = new ArrayList<Employee>();
			listsort = employee.stream().sorted((f,s) -> f.getFirstname().compareTo(s.getFirstname())).collect(Collectors.toList());
			
			return new ResponseEntity<>(listsort, HttpStatus.OK);
		}
		catch(Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("employee/{id}")
	public ResponseEntity<?> getemployeeById(@PathVariable Long id){
		try {
			Employee employee = this.employeerepo.findById(id).orElse(null);
			if(employee != null) {
				return new ResponseEntity<Employee>(employee,HttpStatus.OK);
			}
			else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("employee dengan ID "+ id +" tidak ditemukan");
			}
		} 
		catch (Exception e) {
			return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
			
		}
	}
	
	@PostMapping("addemployee")
	public ResponseEntity<Employee> insertbank(@RequestBody Employee employee) {
		try {
			this.employeerepo.save(employee);
			return new ResponseEntity<Employee>(employee,HttpStatus.OK);
		} 
		catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("checkmail/{check}")
	public ResponseEntity<List<Employee>> checkMail(@PathVariable("check") String check){
		try {
			List<Employee> mail = employeerepo.checking(check);
			return new ResponseEntity<>(mail,HttpStatus.OK);
		} 
		catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			
		}
	}
	
	@PutMapping("editemployee/{id}")
	public ResponseEntity<Employee>editbank(@RequestBody Employee employee, @PathVariable Long id){
		try {
			employee.setId(id);
			this.employeerepo.save(employee);
			return new ResponseEntity<Employee>(employee,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping("deleteemployee/{id}")
	public ResponseEntity<?> deleteemployee(@PathVariable Long id){
		try {
			Employee employee= this.employeerepo.findById(id).orElse(null);
			if(employee!=null) {
				return new ResponseEntity<Employee>(HttpStatus.OK);
			}
			else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal menghapus id" +id+ "Karena tidak ditemukan");
				
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
		}
	}
	


}
