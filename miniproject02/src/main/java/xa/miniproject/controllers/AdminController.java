package xa.miniproject.controllers;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xa.miniproject.models.Admin;
import xa.miniproject.repositories.AdminRepo;

@Controller
@RequestMapping(value="/admin/")
public class AdminController {
	
	@Autowired AdminRepo adminrepo;
	
	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/admin/index");
		List<Admin> listadmin = this.adminrepo.findAll();
		view.addObject("listadmin", listadmin);
		return view;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/admin/form");
		Admin admin = new Admin();
		view.addObject("admin", admin);
		return view;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute Admin admin, BindingResult result) {
		if(!result.hasErrors()) {
			this.adminrepo.save(admin);
		}
		return new ModelAndView("redirect:/admin/index");
	}
	
	@PostMapping(value="ceklogin")
	ModelAndView ceklogin(@ModelAttribute Admin admin, BindingResult result, HttpSession sess) {
		String redirect = "";
		if(!result.hasErrors()) {
			String username = (String) result.getFieldValue("userName");
			String password = (String) result.getFieldValue("password");

			try {				
				List<Admin> getadmin = this.adminrepo.getLogin(username, password);
				try {
					sess.setAttribute("id", getadmin.get(0).getId());
					sess.setAttribute("username", getadmin.get(0).getUserName());
					sess.setAttribute("fullname", getadmin.get(0).getFullname());
					sess.setAttribute("password", getadmin.get(0).getPassword());
					System.out.println("Access Granted!");
					redirect = "redirect:/landingpage";
				} catch (Exception e) {
					System.out.println("Access Denied!");
					redirect = "redirect:/";
				}
			} catch (Exception e) {
				e.printStackTrace();
				redirect = "redirect:/";
			}
		}
		return new ModelAndView(redirect);
	}	
}
