package xa.miniproject.services;

import java.util.List;

import xa.miniproject.models.Employee;

public interface EmployeeService {
	List<Employee>getAllEmployees();

}
