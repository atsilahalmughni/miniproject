package xa.miniproject.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xa.miniproject.models.Employee;
import xa.miniproject.repositories.EmployeeRepo;

@Service
public class EmployeeServicempl implements EmployeeService{
	
	@Autowired
	private EmployeeRepo employeerepo;
	
	@Override
	public List<Employee>getAllEmployees(){
		return employeerepo.findAll();
	}

}
