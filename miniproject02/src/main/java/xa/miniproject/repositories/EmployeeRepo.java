package xa.miniproject.repositories;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import xa.miniproject.models.Employee;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Long> {
	@Query(value="SELECT * FROM employees e WHERE LOWER(e.email)=LOWER(?1)", nativeQuery=true)
	List<Employee> checking(String email);
	
}
