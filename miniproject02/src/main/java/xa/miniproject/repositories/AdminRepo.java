package xa.miniproject.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import xa.miniproject.models.Admin;

@Repository
public interface AdminRepo extends JpaRepository<Admin, Long> {
	@Query(value="SELECT * FROM admin a WHERE a.userName=?1 AND a.password = ?2", nativeQuery=true)
	List<Admin> getLogin(String userName, String password);

}
